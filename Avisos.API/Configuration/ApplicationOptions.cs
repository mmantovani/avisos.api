﻿namespace Avisos.API.Configuration
{
    public class ApplicationOptions
    {
        public static string Section = "Application";
        public string ConnectionString { get; set; }
        public string IdEntidadPrioridad { get; set; }
        public string SpCredito { get; set; }
        public string SpReversa { get; set; }
    }
}
