﻿namespace Avisos.API.Configuration
{
    public class BilleteraOptions
    {
        public static string Section = "Application:Billetera";
        public string BaseUrl { get; set; }
        public string ConnectionString { get; set; }
    }
}
