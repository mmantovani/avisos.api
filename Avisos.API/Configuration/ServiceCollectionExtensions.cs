﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Avisos.API.Configuration
{
    public static class ServiceCollectionExtensions
    {
        public static void AddApplicationConfiguration(this IServiceCollection services)
        {
            IConfiguration configuration;

            using (var serviceScope = services.BuildServiceProvider().CreateScope())
            {
                configuration = serviceScope.ServiceProvider.GetService<IConfiguration>();
            }

            var applicationOptions = new ApplicationOptions();
            configuration.GetSection(ApplicationOptions.Section).Bind(applicationOptions);
            services.AddSingleton(typeof(ApplicationOptions), applicationOptions);
        }
        public static void AddBilleteraConfiguration(this IServiceCollection services)
        {
            IConfiguration configuration;

            using (var serviceScope = services.BuildServiceProvider().CreateScope())
            {
                configuration = serviceScope.ServiceProvider.GetService<IConfiguration>();
            }

            var billeteraOptions = new BilleteraOptions();
            configuration.GetSection(BilleteraOptions.Section).Bind(billeteraOptions);
            services.AddSingleton(typeof(BilleteraOptions), billeteraOptions);
        }
    }
}