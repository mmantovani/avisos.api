﻿using Avisos.API.Helpers;
using Avisos.API.Models;
using Avisos.API.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Avisos.API.Controllers
{
    [ApiController]

    public class AvisosController : ControllerBase
    {
        private readonly ILogger<AvisosController> _logger;
        private readonly IAvisoCreditoService _avisoCreditoService;

        public AvisosController(ILogger<AvisosController> logger,
                                IAvisoCreditoService avisoCreditoService)
        {
            _logger = logger;
            _avisoCreditoService = avisoCreditoService;
        }

        [HttpPost]
        [Route("AvisoCreditoCVU")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(AvisoCreditoResponse), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadGateway)]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        public async Task<IActionResult> AvisoCreditoAsync([FromBody] AvisoCreditoRequest avisoCreditoRequest)
        {
            AvisoCreditoResponse response = null;
            try
            {
                var idEntidad = _avisoCreditoService.GetEntidad(avisoCreditoRequest.Cvu);
                var resultado = _avisoCreditoService.InsertAvisoCreditoToDB(avisoCreditoRequest, idEntidad);

                switch (resultado)
                {
                    case (int)Enums.SP.CodOK:
                        response = new AvisoCreditoResponse()
                        {
                            PCodRespuesta = "200",
                            PDescRespuesta = "ok"
                        };
                        break;
                    case (int)Enums.SP.CodError:
                        response = new AvisoCreditoResponse()
                        {
                            PCodRespuesta = "400",
                            PDescRespuesta = "Error al insertar el aviso"
                        };
                        break;                                     
                }                
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);

                response = new AvisoCreditoResponse()
                {
                    PCodRespuesta = "400",
                    PDescRespuesta = e.Message
                };
            }
            return Ok(response);
        }

        [HttpPost]
        [Route("AvisoReversaDebito")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(type: typeof(AvisoOperacionFinalizadaResponse), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadGateway)]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        public async Task<IActionResult> AvisoOperacionFinalizadaAsync([FromBody] AvisoOperacionFinalizadaRequest avisoOperacionFinalizadaRequest)
        {
            AvisoOperacionFinalizadaResponse response = null;
            try
            {
                var resultado = _avisoCreditoService.InsertAvisoReversaToDB(avisoOperacionFinalizadaRequest);

                switch (resultado)
                {
                    case (int)Enums.SP.CodOK:
                        response = new AvisoOperacionFinalizadaResponse()
                        {
                            PCodRespuesta = "200",
                            PDescRespuesta = "ok"
                        };
                        break;
                    case (int)Enums.SP.CodError:
                        response = new AvisoOperacionFinalizadaResponse()
                        {
                            PCodRespuesta = "400",
                            PDescRespuesta = "Error al insertar el aviso"
                        };
                        break;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);

                response = new AvisoOperacionFinalizadaResponse()
                {
                    PCodRespuesta = "400",
                    PDescRespuesta = e.Message
                };
            }
            return Ok(response);
        }
    }
}
