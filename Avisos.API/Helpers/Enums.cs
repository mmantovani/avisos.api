﻿namespace Avisos.API.Helpers
{
    public class Enums
    {
        public enum CreditoInformado
        {
            Pendiente = 1,
            Procesado = 2,
            Error = 3,
            Procesando = 4,
            PendienteConPrioridad = 11,
            ProcesandoConPrioridad = 41,
            NoProcesar = 6
                
        }

        public enum TipoAvisos
        {
            CreditoCVU = 1,
            ReversaDebito = 2
        }
        public enum SP
        {
            CodOK = 200,
            CodError = 400,
            CodDuplicado = 500
        }
    }
}
