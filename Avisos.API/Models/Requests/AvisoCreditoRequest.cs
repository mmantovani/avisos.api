﻿using System;

namespace Avisos.API.Models
{
    public class AvisoCreditoRequest
    {
        public long Id { get; set; }
        public string IdTrxOriginal { get; set; }
        public string Cbu { get; set; }
        public DateTime FechaNegocio { get; set; }
        public string IdPsp { get; set; }
        public string Moneda { get; set; }
        public decimal Importe { get; set; }
        public string Cvu { get; set; }
        public string CuitCvu { get; set; }
        public string TitularCvu { get; set; }
        public string CuitOriginante { get; set; }
        public string CuentaOriginante { get; set; }
        public string TitularOriginante { get; set; }
        public string Concepto { get; set; }
        public string Descripcion { get; set; }
        public string IdCoelsa { get; set; }
    }
}
