﻿using System;

namespace Avisos.API.Models
{
    public class AvisoOperacionFinalizadaRequest
    {
        public long Id { get; set; }
        public long? IdAnulacion { get; set; }
        public string Cbu { get; set; }
        public DateTime? FechaNegocio { get; set; }
        public string Moneda { get; set; }
        public decimal? Importe { get; set; }
        public string IdPsp { get; set; }
        public string CuitPsp { get; set; }
        public string Cvu { get; set; }
        public string CuitCvu { get; set; }
        public string TitularCvu { get; set; }
    }
}