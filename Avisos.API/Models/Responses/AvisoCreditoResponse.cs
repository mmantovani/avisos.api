﻿namespace Avisos.API.Models
{
    public class AvisoCreditoResponse
    {
        public string PCodRespuesta { get; set; }
        public string PDescRespuesta { get; set; }
    }
}
