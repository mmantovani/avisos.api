﻿namespace Avisos.API.Models
{
    public class AvisoOperacionFinalizadaResponse
    {
        public string PCodRespuesta { get; set; }
        public string PDescRespuesta { get; set; }
    }
}
