﻿using System.Collections.Generic;

namespace Avisos.API.Models
{
    public class IdResponse
    {
        public List<int> Id { get; set; }
    }
}
