USE [avisos_dev]
GO
/****** Object:  Table [dbo].[AvisosCreditoCVU]    Script Date: 11/4/2022 16:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AvisosCreditoCVU](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdWeb] [uniqueidentifier] NOT NULL DEFAULT NEWID(),
	[FechaAviso] [datetime] NULL,
	[FechaRecepcion] [datetime] NULL,
	[FechaProcesado] [datetime] NULL,
	[RequestJson] [varchar](max) NOT NULL,
	[Estado] [smallint] NULL DEFAULT 1,
	[Detalles] [varchar](50) NULL,
	[IdAvisoCoelsa] [bigint] NULL,
	[IdTrxOriginal] [bigint] NULL,
	[IdCoelsa] [varchar](50) NOT NULL UNIQUE,
	[IdEntidad] [int] NULL,
	[Cbu] [varchar](30) NULL,
	[Importe] [decimal](18, 4) NULL,
	[IdPsp] [varchar](10) NULL,
	[Cvu] [varchar](30) NULL,
	[CuitCvu] [varchar](20) NULL,
	[TitularCvu] [varchar](100) NULL,
	[CuitOriginante] [varchar](20) NULL,
	[CuentaOriginante] [varchar](30) NULL,
	[TitularOriginante] [varchar](100) NULL,
	[Concepto] [varchar](50) NULL,
	[Descripcion] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AvisosReversaDebito]    Script Date: 11/4/2022 16:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AvisosReversaDebito](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdWeb] [uniqueidentifier] NOT NULL DEFAULT NEWID(),
	[FechaAviso] [datetime] NULL,
	[FechaRecepcion] [datetime] NULL,
	[FechaProcesado] [datetime] NULL,
	[RequestJson] [varchar](max) NOT NULL,
	[Estado] [smallint] NULL DEFAULT 1,
	[Detalles] [varchar](50) NULL,
	[IdAvisoCoelsa] [bigint] NULL,
	[IdAnulacion] [bigint] NOT NULL UNIQUE,
	[IdEntidad] [int] NULL,
	[Cbu] [varchar](30) NULL,
	[Importe] [decimal](18, 4) NULL,
	[IdPsp] [varchar](10) NULL,
	[CuitPsp] [varchar](20) NULL,
	[Cvu] [varchar](30) NULL,
	[CuitCvu] [varchar](20) NULL,
	[TitularCvu] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Estados]    Script Date: 11/4/2022 16:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estados](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdWeb] [uniqueidentifier] NULL,
	[Descripcion] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

SET IDENTITY_INSERT [dbo].[Estados] ON 
INSERT [dbo].[Estados] ([Id], [IdWeb], [Descripcion]) VALUES (1, N'9924142b-4fcc-42f3-8c75-7be7aadd3ed6', N'Pendiente')
INSERT [dbo].[Estados] ([Id], [IdWeb], [Descripcion]) VALUES (2, N'04c94c50-d4a1-4609-84a3-a3e4066a92c7', N'Procesado')
INSERT [dbo].[Estados] ([Id], [IdWeb], [Descripcion]) VALUES (3, N'8d37eccf-f23e-4412-bfb1-7c28d504acc3', N'Error')
INSERT [dbo].[Estados] ([Id], [IdWeb], [Descripcion]) VALUES (4, N'f0063b52-96c7-4d0f-b19b-3aee599572db', N'Procesando')
INSERT [dbo].[Estados] ([Id], [IdWeb], [Descripcion]) VALUES (11, N'71e0ac8f-a344-4591-9cf1-ece854cacdc7', N'PrendienteConPrioridad')
INSERT [dbo].[Estados] ([Id], [IdWeb], [Descripcion]) VALUES (41, N'7b3142f1-2cd6-4414-86a4-eabe159ac137', N'ProcesandoConPrioridad')
INSERT [dbo].[Estados] ([Id], [IdWeb], [Descripcion]) VALUES (6, N'71f11ce0-a22a-4860-acdf-9b75874c9bd1', N'NoProcesar')



SET IDENTITY_INSERT [dbo].[Estados] OFF
GO

/****** Object:  StoredProcedure [dbo].[sp_InsertAvisoCreditoCVU]    Script Date: 11/4/2022 16:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_InsertAvisoCreditoCVU]
	@pFechaAviso		DATETIME,
	@pFechaRecepcion	DATETIME,
	@pRequestJson		VARCHAR(MAX),
	@pEstado			SMALLINT,
	@pDetalles			VARCHAR(50),
	@pIdAvisoCoelsa		BIGINT,
    @pIdTrxOriginal		BIGINT,
    @pIdCoelsa			VARCHAR(50),
    @pCbu				VARCHAR(30),
    @pImporte			DECIMAL(18,4),
    @pIdPsp				VARCHAR(10),
    @pCvu				VARCHAR(30),
    @pCuitCvu			VARCHAR(20),
    @pTitularCvu		VARCHAR(100),
    @pCuitOriginante	VARCHAR(20),
    @pCuentaOriginante	VARCHAR(30),
    @pTitularOriginante VARCHAR(100),
    @pConcepto			VARCHAR(50),
    @pDescripcion		VARCHAR(50),
	@pIdEntidad			INT

AS
BEGIN
	
	SET NOCOUNT ON;
	BEGIN TRY

		INSERT INTO [dbo].[AvisosCreditoCVU] 
				(FechaAviso,
				FechaRecepcion, 
				RequestJson,
				Estado,
				Detalles,
				IdAvisoCoelsa,		
				IdTrxOriginal,	
				IdCoelsa,			
				Cbu,				
				Importe,			
				IdPsp,				
				Cvu,				
				CuitCvu,			
				TitularCvu,		
				CuitOriginante,	
				CuentaOriginante,	
				TitularOriginante, 
				Concepto,			
				Descripcion,
				IdEntidad)

		VALUES (@pFechaAviso,
				@pFechaRecepcion,
				@pRequestJson,
				@pEstado,
				@pDetalles,
				@pIdAvisoCoelsa,		
				@pIdTrxOriginal,		
				@pIdCoelsa,			
				@pCbu,				
				@pImporte,			
				@pIdPsp,				
				@pCvu,				
				@pCuitCvu,			
				@pTitularCvu,		
				@pCuitOriginante,	
				@pCuentaOriginante,	
				@pTitularOriginante, 
				@pConcepto,			
				@pDescripcion,
				@pIdEntidad);

		RETURN 200;

	END TRY
	BEGIN CATCH
		RETURN 400;
	END CATCH    
END
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertAvisoReversaDebito]    Script Date: 11/4/2022 16:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_InsertAvisoReversaDebito]
	@pFechaAviso		DATETIME,
	@pFechaRecepcion	DATETIME,
	@pRequestJson		VARCHAR(MAX),
	@pEstado			SMALLINT,
	@pDetalles			VARCHAR(50),
	@pIdAvisoCoelsa		BIGINT,
	@pIdAnulacion		BIGINT,
	@pCbu				VARCHAR(30),
	@pImporte			DECIMAL(18,4),
	@pIdPsp				VARCHAR(10),
	@pCuitPsp			VARCHAR(20),
	@pCvu				VARCHAR(30),
	@pCuitCvu			VARCHAR(20),
	@pTitularCvu		VARCHAR(100)

AS
BEGIN
		
	BEGIN TRY

		INSERT INTO [dbo].[AvisosReversaDebito] 
				(FechaAviso,
				FechaRecepcion, 
				RequestJson,
				Estado,
				Detalles,
				IdAvisoCoelsa,		
				IdAnulacion,	
				Cbu,				
				Importe,			
				IdPsp,				
				Cvu,				
				CuitCvu,			
				TitularCvu)

		VALUES (@pFechaAviso,
				@pFechaRecepcion,
				@pRequestJson,
				@pEstado,
				@pDetalles,
				@pIdAvisoCoelsa,		
				@pIdAnulacion,			
				@pCbu,				
				@pImporte,			
				@pIdPsp,				
				@pCvu,				
				@pCuitCvu,			
				@pTitularCvu);

		RETURN 200;

	END TRY
	BEGIN CATCH
		RETURN 400;
	END CATCH
   
END
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateAvisoCreditoCVU]    Script Date: 13/4/2022 12:14:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_UpdateAvisoCreditoCVU]
	@pId			 BIGINT,
	@pCodigo		 VARCHAR(5),
	@pRespuesta		 VARCHAR(50),
	@pFechaProcesado DATETIME
	
AS
BEGIN
	DECLARE
	@vCodigoInterno SMALLINT;

	IF(@pCodigo = '200')
		SET @vCodigoInterno = 2
	ELSE
		SET @vCodigoInterno = 3

	SET NOCOUNT ON;
	BEGIN TRY
	
	UPDATE [dbo].[AvisosCreditoCVU]
	SET Estado = @vCodigoInterno,
		Detalles = @pRespuesta,
		FechaProcesado = @pFechaProcesado
	WHERE IdAvisoCoelsa = @pId;

		RETURN 200;

	END TRY
	BEGIN CATCH
		RETURN 400;
	END CATCH
    
END
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateAvisoReversaDebito]    Script Date: 13/4/2022 12:15:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_UpdateAvisoReversaDebito]
	@pId			 BIGINT,
	@pCodigo		 VARCHAR(5),
	@pRespuesta	     VARCHAR(50),
	@pFechaProcesado DATETIME
	
AS
BEGIN
	DECLARE
	@vCodigoInterno SMALLINT;

	IF(@pCodigo = '200')
		SET @vCodigoInterno = 2
	ELSE
		SET @vCodigoInterno = 3

	SET NOCOUNT ON;
	BEGIN TRY
	
	UPDATE [dbo].[AvisosReversaDebito]
	SET Estado = @vCodigoInterno,
		Detalles = @pRespuesta,
		FechaProcesado = @pFechaProcesado
	WHERE IdAvisoCoelsa = @pId;

		RETURN 200;

	END TRY
	BEGIN CATCH
		RETURN 400;
	END CATCH
    
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SelectAvisosCreditoCVU]    Script Date: 6/5/2022 15:45:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_SelectAvisosCreditoCVU]
	@pTop				INT
    
AS
BEGIN
	DECLARE @sqlstatement NVARCHAR(MAX)

	IF(@pTop <> 0)
	BEGIN
		SET @sqlstatement = 'SELECT TOP ' + CONVERT(VARCHAR(4),CAST(@pTop AS VARCHAR)) + ' * INTO #avisos FROM [dbo].[AvisosCreditoCVU] WHERE Estado = 1;
							 UPDATE [dbo].[AvisosCreditoCVU] SET Estado = 4 WHERE Id IN (SELECT Id FROM #avisos);
							 SELECT RequestJson FROM #avisos;'
    END
	ELSE
	BEGIN
		SET @sqlstatement = 'SELECT TOP 1000 * INTO #avisos FROM [dbo].[AvisosCreditoCVU] WHERE Estado = 1;
							 UPDATE [dbo].[AvisosCreditoCVU] SET Estado = 4 WHERE Id IN (SELECT Id FROM #avisos);
							 SELECT RequestJson FROM #avisos;'
	END

	EXECUTE sp_executesql @sqlstatement	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SelectAvisosCreditoCVUPrioritario]    Script Date: 6/5/2022 15:54:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_SelectAvisosCreditoCVUPrioritario]
	@pTop				INT
    
AS
BEGIN
	DECLARE @sqlstatement NVARCHAR(MAX)

	IF(@pTop <> 0)
	BEGIN
		SET @sqlstatement = 'SELECT TOP ' + CONVERT(VARCHAR(4),CAST(@pTop AS VARCHAR)) + ' * INTO #avisos FROM [dbo].[AvisosCreditoCVU] WHERE Estado = 11;
							 UPDATE [dbo].[AvisosCreditoCVU] SET Estado = 41 WHERE Id IN (SELECT Id FROM #avisos);
							 SELECT RequestJson FROM #avisos;'
    END
	ELSE
	BEGIN
		SET @sqlstatement = 'SELECT TOP 1000 * INTO #avisos FROM [dbo].[AvisosCreditoCVU] WHERE Estado = 11;
							 UPDATE [dbo].[AvisosCreditoCVU] SET Estado = 41 WHERE Id IN (SELECT Id FROM #avisos);
							 SELECT RequestJson FROM #avisos;'
	END

	EXECUTE sp_executesql @sqlstatement

END
GO
/****** Object:  StoredProcedure [dbo].[sp_SelectAvisosCreditoCVU]    Script Date: 6/5/2022 15:45:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_SelectAvisosReversaDebito]
	@pTop				INT
    
AS
BEGIN
	DECLARE @sqlstatement NVARCHAR(MAX)

	IF(@pTop <> 0)
	BEGIN
		SET @sqlstatement = 'SELECT TOP ' + CONVERT(VARCHAR(4),CAST(@pTop AS VARCHAR)) + ' * INTO #avisos FROM [dbo].[AvisosReversaDebito] WHERE Estado = 1;
							 UPDATE [dbo].[AvisosReversaDebito] SET Estado = 4 WHERE Id IN (SELECT Id FROM #avisos);
							 SELECT RequestJson FROM #avisos;'
    END
	ELSE
	BEGIN
		SET @sqlstatement = 'SELECT TOP 1000 * INTO #avisos FROM [dbo].[AvisosReversaDebito] WHERE Estado = 1;
							 UPDATE [dbo].[AvisosReversaDebito] SET Estado = 4 WHERE Id IN (SELECT Id FROM #avisos);
							 SELECT RequestJson FROM #avisos;'
	END

	EXECUTE sp_executesql @sqlstatement	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SelectAvisosCreditoCVUPrioritario]    Script Date: 6/5/2022 15:54:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_SelectAvisosReversaDebitoPrioritario]
	@pTop				INT
    
AS
BEGIN
	DECLARE @sqlstatement NVARCHAR(MAX)

	IF(@pTop <> 0)
	BEGIN
		SET @sqlstatement = 'SELECT TOP ' + CONVERT(VARCHAR(4),CAST(@pTop AS VARCHAR)) + ' * INTO #avisos FROM [dbo].[AvisosReversaDebito] WHERE Estado = 11;
							 UPDATE [dbo].[AvisosReversaDebito] SET Estado = 41 WHERE Id IN (SELECT Id FROM #avisos);
							 SELECT RequestJson FROM #avisos;'
    END
	ELSE
	BEGIN
		SET @sqlstatement = 'SELECT TOP 1000 * INTO #avisos FROM [dbo].[AvisosReversaDebito] WHERE Estado = 11;
							 UPDATE [dbo].[AvisosReversaDebito] SET Estado = 41 WHERE Id IN (SELECT Id FROM #avisos);
							 SELECT RequestJson FROM #avisos;'
	END

	EXECUTE sp_executesql @sqlstatement

END
GO











