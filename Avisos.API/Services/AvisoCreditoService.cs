﻿using Avisos.API.Configuration;
using Avisos.API.Helpers;
using Avisos.API.Models;
using Dapper;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Avisos.API.Services
{
    public class AvisoCreditoService : IAvisoCreditoService
    {
        private readonly ApplicationOptions _applicationOptions;
        private readonly BilleteraOptions _billeteraOptions;

        private readonly ILogger<AvisoCreditoService> _logger;
        private int _result;

        public AvisoCreditoService(  ApplicationOptions applicationOptions,
                                     ILogger<AvisoCreditoService> logger,
                                     BilleteraOptions billeteraOptions)
        {
            _applicationOptions = applicationOptions;
            _billeteraOptions = billeteraOptions;
            _logger = logger;
            _result = 0;
        }
        public int? GetEntidad(string Cvu)
        {         
            int? result = 0;
            
            string sql =$"SELECT e.Id FROM Entidades e " +
                        $"JOIN Usuarios u ON u.IdEntidad = e.Id " +
                        $"JOIN Usuarios_EntidadesLineas ue ON ue.IdUsuario = u.Id " +
                        $"JOIN Usuarios_EntidadesLineasCuentas uec ON uec.IdUsuarioEntidadLinea = ue.Id " +
                        $"WHERE uec.CVU = '{Cvu}'";

            using (var connection = new SqlConnection(_billeteraOptions.ConnectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sql, connection);

                object v = command.ExecuteScalar();
                result = (int)v;
                connection.Close();                 
            }

            return result;
        }
        public int InsertAvisoCreditoToDB(AvisoCreditoRequest avisoCredito, int? idEntidad)
        {
            var estado = GetEntidadPrioritario(idEntidad);

            var procedimiento = _applicationOptions.SpCredito;
            var parameters = new DynamicParameters();

            parameters.Add("@pFechaAviso", avisoCredito.FechaNegocio);
            parameters.Add("@pFechaRecepcion", DateTime.Now);
            parameters.Add("@pRequestJson", JsonConvert.SerializeObject(avisoCredito));
            parameters.Add("@pEstado", estado);
            parameters.Add("@pDetalles", null);
            parameters.Add("@pIdAvisoCoelsa", avisoCredito.Id);
            parameters.Add("@pIdTrxOriginal", avisoCredito.IdTrxOriginal);
            parameters.Add("@pIdCoelsa", avisoCredito.IdCoelsa);
            parameters.Add("@pCbu", avisoCredito.Cbu);
            parameters.Add("@pImporte", avisoCredito.Importe);
            parameters.Add("@pIdPsp", avisoCredito.IdPsp);
            parameters.Add("@pCvu", avisoCredito.Cvu);
            parameters.Add("@pCuitCvu", avisoCredito.CuitCvu);
            parameters.Add("@pTitularCvu", avisoCredito.TitularCvu);
            parameters.Add("@pCuitOriginante", avisoCredito.CuitOriginante);
            parameters.Add("@pCuentaOriginante", avisoCredito.CuentaOriginante);
            parameters.Add("@pTitularOriginante", avisoCredito.TitularOriginante);
            parameters.Add("@pDescripcion", avisoCredito.Descripcion);
            parameters.Add("@pConcepto", avisoCredito.Concepto);
            parameters.Add("@pIdEntidad", idEntidad);
            parameters.Add("@result", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

            using (var connection = new SqlConnection(_applicationOptions.ConnectionString))
            {
                connection.Query(procedimiento, parameters, commandType: CommandType.StoredProcedure, commandTimeout: 50);
                _result = parameters.Get<int>("@result");
            }            
            return _result;    
        }
        public int InsertAvisoReversaToDB(AvisoOperacionFinalizadaRequest avisoOperacionFinalizada)
        {
            var procedimiento = _applicationOptions.SpReversa;
            var parameters = new DynamicParameters();

            parameters.Add("@pFechaAviso", avisoOperacionFinalizada.FechaNegocio);
            parameters.Add("@pFechaRecepcion", DateTime.Now);
            parameters.Add("@pRequestJson", JsonConvert.SerializeObject(avisoOperacionFinalizada));
            parameters.Add("@pEstado", Enums.CreditoInformado.Pendiente);
            parameters.Add("@pDetalles", null);
            parameters.Add("@pIdAvisoCoelsa", avisoOperacionFinalizada.Id);
            parameters.Add("@pIdAnulacion", avisoOperacionFinalizada.IdAnulacion);
            parameters.Add("@pCbu", avisoOperacionFinalizada.Cbu);
            parameters.Add("@pImporte", avisoOperacionFinalizada.Importe);
            parameters.Add("@pIdPsp", avisoOperacionFinalizada.IdPsp);
            parameters.Add("@pCuitPsp", avisoOperacionFinalizada.CuitPsp);
            parameters.Add("@pCvu", avisoOperacionFinalizada.Cvu);
            parameters.Add("@pCuitCvu", avisoOperacionFinalizada.CuitCvu);
            parameters.Add("@pTitularCvu", avisoOperacionFinalizada.TitularCvu);
            parameters.Add("@result", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
              
            using (var connection = new SqlConnection(_applicationOptions.ConnectionString))
            {
                connection.Query(procedimiento, parameters, commandType: CommandType.StoredProcedure, commandTimeout: 50);
                _result = parameters.Get<int>("@result");
            }
            return _result;
        }

        private Enums.CreditoInformado GetEntidadPrioritario(int? IdEntidad)
        {
            var id = JsonConvert.DeserializeObject<IdResponse>(_applicationOptions.IdEntidadPrioridad);
            
            if (id == null)
                return Enums.CreditoInformado.Pendiente;

            foreach(var entidad in id.Id)
            {
                if(entidad == IdEntidad)
                    return Enums.CreditoInformado.PendienteConPrioridad;
            }
            return Enums.CreditoInformado.Pendiente;
        }
    }
}

  

