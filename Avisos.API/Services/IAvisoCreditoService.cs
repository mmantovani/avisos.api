﻿using Avisos.API.Models;

namespace Avisos.API.Services
{
    public interface IAvisoCreditoService
    {
        int InsertAvisoCreditoToDB(AvisoCreditoRequest avisoCreditoRequest, int? idEntidad);
        int InsertAvisoReversaToDB(AvisoOperacionFinalizadaRequest avisoOperacionFinalizadaRequest);
        int? GetEntidad(string Cvu);
    }
}
