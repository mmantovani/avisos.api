﻿#addin nuget:?package=Cake.Git&version=0.21.0

///////////////////////////////////////////////////////////////////////////////
// ARGUMENTS
///////////////////////////////////////////////////////////////////////////////

var target = Argument("target", "Build");
var configuration = Argument("configuration", "Release");
var prefixVersion = "1.0";
var newVersionNumber = "";
var project = "Avisos.Api";

var baseDirectory = Directory("./output/release/");
var outputDirectory = baseDirectory;
var distDirectory = "";

///////////////////////////////////////////////////////////////////////////////
// SETUP / TEARDOWN
///////////////////////////////////////////////////////////////////////////////

Setup(ctx =>
{
    var tags = GitTags(".");
    var newBuildNumber = 0;
    if (tags.Count() > 0){
        var lastVersionNumber = tags
        .Select(x => x.ToString().Split('/')[2])
        .Where(x => x.StartsWith(prefixVersion))
        .OrderBy(x => new Version(x))
        .Last();
        
        if ((lastVersionNumber.Split('.')[0] == prefixVersion.Split('.')[0]) && (lastVersionNumber.Split('.')[1] == prefixVersion.Split('.')[1]))
        {
            var lastBuildNumber = lastVersionNumber.Split('.')[2];
            newBuildNumber = Convert.ToInt32(lastBuildNumber) + 1;
        }
    }
    
    newVersionNumber = prefixVersion + "." + newBuildNumber;

    outputDirectory = outputDirectory + Directory(newVersionNumber);
    distDirectory  = outputDirectory + Directory("dist");

    // Executed BEFORE the first task.
    Information("Running tasks...");
    Information("Building Version: " + newVersionNumber);
});

Teardown(ctx =>
{
    GitTag(".", newVersionNumber);
    //GitPushRef(".", "origin", "/refs/tags/" + newVersionNumber); 

    // Executed AFTER the last task.
    Information("Finished running tasks.");
});

///////////////////////////////////////////////////////////////////////////////
// TASKS
///////////////////////////////////////////////////////////////////////////////

Task("Clean").Does(() => 
{ 
    CleanDirectory(distDirectory); 
});

Task("Restore")
    .Does(() =>
    {
        DotNetCoreRestore();
    });

Task("Build")
    .IsDependentOn("Clean")
    .IsDependentOn("Restore")
    .Does(() =>
    {
        var settings = new DotNetCorePublishSettings
        {
            Configuration = configuration,
            OutputDirectory = distDirectory,
            MSBuildSettings = new DotNetCoreMSBuildSettings()
        };
        settings.MSBuildSettings.SetVersion(newVersionNumber);
        DotNetCorePublish("./" + project + ".sln", settings);

        CopyFiles(".dockerignore", outputDirectory);
        CopyFiles("Dockerfile", outputDirectory);

        DeleteFiles(distDirectory + "/appsettings.*.json");
        var zipFile =  baseDirectory + File($"{project}-{newVersionNumber}.zip");

        Zip(outputDirectory, zipFile);

        DeleteDirectory(outputDirectory, new DeleteDirectorySettings {
            Recursive = true,
            Force = true
        });
    });

RunTarget(target);
